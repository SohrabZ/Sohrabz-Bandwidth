var Bandwidth = require('node-bandwidth')
var express = require('express')
var bodyParser = require('body-parser')
var xml = require('xml')
var app = express()

var client = new Bandwidth({
  userId    : 'u-xftfuhswvgo5s5z5nktys6q',
  apiToken  : 't-iar2zxxwwlz4b5ydbd2fy6y',
  apiSecret : 'qgfeseommy6muqq2c6xibp6fyhl7m7bc5vg263y'
})

var admin_phone = '+14243942058'

app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Success !!!')
})

app.post('/message', function(req, res) {
  var event_type     = req.body.eventType
  var direction      = req.body.direction
  var message_id     = req.body.messageId
  var message_uri    = req.body.messageUri
  var from           = req.body.from
  var to             = req.body.to
  var text           = req.body.text
  var application_id = req.body.applicationId
  var time           = req.body.time
  var state          = req.body.state

  console.log(JSON.stringify(req.body))

  var message = {
    from: to,
    to: admin_phone,
    text: 'Transfered from: ' + from + '\nMessage: ' + text
  }

  client.Message.send(message)
  .then(function(message) {
    console.log('Message transfered with ID: ' + message.id)
  })
  .catch(function(err) {
    console.log(err.message)
  })
})

app.post('/voice', function(req, res) {
  var event_type       = req.body.eventType
  var from             = req.body.from
  var to               = req.body.to
  var call_id          = req.body.callId
  var call_uri         = req.body.callUri
  var call_state       = req.body.callState
  var application_id   = req.body.applicationId
  var time             = req.body.time
  var cause            = req.body.cause
  var recording_id     = req.body.recordingId
  var status           = req.body.status
  var transcription_id = req.body.transcriptionId

  // console.log(JSON.stringify(req.body))

  if(event_type == 'incomingcall') {
    console.log('Incoming call from: ' + from + ' to: ' + to)
    res.sendStatus(200)
  } else if(event_type == 'answer') {
    var speakSentence = {
      transferTo       : admin_phone,
      whisperAudio     : {
        sentence : 'You have an incoming call from call sense',
        gender   : 'female',
        voice    : 'julie',
        locale   : 'en'
      }
    }

    client.Call.transfer(call_id, speakSentence)
    .then(function (call) {
      console.log('Call transfered with ID: ' + call.id)
      // client.Call.enableRecording(call_id)
      // .then(function () {
      //   console.log('Recording started')
      // })
      // .catch(function(err) {
      //   console.log(err.message)
      // })
    })
    .catch(function(err) {
      console.log(err.message)
    })
    
  } else if(event_type == 'hangup') {
    console.log('Call hunged from: ' + from + ' to: ' + to + ' cause: ' + cause)
    res.sendStatus(200)
  } else if(event_type == 'recording' && status == 'complete') {
    // client.Recording.createTranscription(recording_id)
    // .then(function () {
    //   console.log('Transcribing started')
    // })
    // .catch(function(err) {
    //   console.log(err.message)
    // })
  } else if (event_type == 'transcription' && status == 'completed') {
    // client.Recording.getTranscription(recording_id, transcription_id)
    // .then(function(transcription){
    //   console.log('Transcript:\n')
    //   console.log(transcription.text)
    // })
    // .catch(function(err) {
    //   console.log(err.message)
    // })
  }

})

app.get('/voiceBXML', function(req, res) {
  var event_type       = req.query.eventType
  var from             = req.query.from
  var to               = req.query.to
  var call_id          = req.query.callId
  var call_uri         = req.query.callUri
  var call_state       = req.query.callState
  var application_id   = req.query.applicationId
  var time             = req.query.time
  var cause            = req.query.cause
  var recording_id     = req.query.recordingId
  var status           = req.query.status
  var transcription_id = req.query.transcriptionId

  console.log(JSON.stringify(req.query))

  if(event_type == 'answer') {
    console.log('Incoming call from: ' + from + ' to: ' + to)

    var myApp = new Bandwidth.BXMLResponse()
    myApp
    .transfer({
      transferTo: admin_phone
    }, function () {
      this.speakSentence("You have an incoming call from call sense",
      {
        gender   : 'female',
        voice    : 'julie',
        locale   : 'en'
      })
    })

    console.log(myApp.toString())

    res.set('Content-Type', 'text/xml')
    res.send(myApp.toString())
  } else {
    res.sendStatus(200)
  }
})

app.listen(3000, () => console.log('Server running on port 3000'))
