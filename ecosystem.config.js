module.exports = {
  apps : [

    {
      name      : 'bandwidth',
      script    : './index.js',
    },
  ],

  deploy : {
    production : {
      user : 'ubuntu',
      host : 'ec2-54-227-229-176.compute-1.amazonaws.com',
      ref  : 'origin/master',
      key: '~/.ssh/aws-free.pem',
      repo : 'git@gitlab.com:SohrabZ/Sohrabz-Bandwidth.git',
      path : '/home/ubuntu/server',
      'post-deploy' : 'npm install && pm2 startOrRestart ecosystem.config.js'
    },
  }
};
